package com.yung.proxy.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.littleshoot.proxy.HttpProxyServer;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;

public class ProxyListener implements ServletContextListener {
    
    private static HttpProxyServer server;
    
    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        
        try {
            System.out.println("stop proxy");
            server.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        
        try {
            System.out.println("start proxy");
            server = DefaultHttpProxyServer.bootstrap().withPort(9090).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

}
